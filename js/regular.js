let regularTel = /(^\+)([0-9]\d{1,1})([(])?([0-9]\d{1,2})([)])?([0-9]\d{1,6})/;
let regularMail = /^([a-zA-Z\-0-9]+)@([a-zA-Z\-0-9]+)\.([a-zA-Z]{2,3})$/;
let regularHttps = /(^http(s)?:\/\/)([a-zA-Z\-0-9]+)\.([a-zA-Z]{2,3})$/;
let regularPassword = /(([a-zA-z]+[\_0-9])|([0-9]+[a-zA-z]+)?([0-9])?){5,25}/;
let regularIpv4 = /([0-9]){1,3}\.([0-9]){1,3}\.([0-9]){1,3}\.([0-9]){1,3}/;