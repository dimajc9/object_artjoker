let positionNumber = {
    Head: 1,
    bartender: 2,
    chef: 3,
    loader: 4,
    sorter: 5,
    waiter: 6,
    сook: 7,
}

let employeeObjectss = [
    {
        id: 1,
        name: 'Alex',
        surname: 'Alexis',
        position: 'waiter',
        positionNumber: 6,
        salary: 1200,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        id: 2,
        name: 'Aleksandr',
        surname: 'Hoggarth',
        position: 'bartender',
        positionNumber: 2,
        salary: 1500,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        id: 3,
        name: 'Kirill',
        surname: 'Nash',
        position: 'bartender',
        positionNumber: 2,
        salary: 1600,
        works: false,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        id: 4,
        name: 'Aleksey',
        surname: 'Holiday',
        position: 'waiter',
        positionNumber: 6,
        salary: 1100,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    },
    {
        id: 5,
        name: 'Anatoly',
        surname: 'James',
        position: 'Head of department',
        positionNumber: 1,
        salary: 2100,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        id: 6,
        name: 'Aleksey',
        surname: 'Keat',
        position: 'waiter',
        positionNumber: 6,
        salary: 1100,
        works: false,
        departmentNumber: 1,
        departmentName: 'hall',
    },
    {
        id: 7,
        name: 'Artur',
        surname: 'Kendal',
        position: 'Head of department',
        positionNumber: 1,
        salary: 2400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    },
    {
        id: 8,
        name: 'Artem',
        surname: 'Kelly',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        id: 9,
        name: 'Boris',
        surname: 'Kennedy',
        position: 'сook',
        positionNumber: 7,
        salary: 1450,
        works: false,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        id: 10,
        name: 'Vadim',
        surname: 'Kennett',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        id: 11,
        name: 'Valentin',
        surname: 'Kingsman',
        position: 'сook',
        positionNumber: 7,
        salary: 1300,
        works: false,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        id: 12,
        name: 'Valeriy',
        surname: 'Kirk',
        position: 'chef',
        positionNumber: 3,
        salary: 1800,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        id: 13,
        name: 'Mikhail',
        surname: 'Nevill',
        position: 'chef',
        positionNumber: 3,
        salary: 2100,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        id: 14,
        name: 'Vasily',
        surname: 'Laird',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        id: 15,
        name: 'Viktor',
        surname: 'Lamberts',
        position: 'sorter',
        positionNumber: 5,
        salary: 1000,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        id: 16,
        name: 'Vitaly',
        surname: 'Larkins',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        id: 17,
        name: 'Vladimir',
        surname: 'Lawman',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        id: 18,
        name: 'Gleb',
        surname: 'Leman',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: false,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        id: 19,
        name: 'Grigory',
        surname: 'Macey',
        position: 'sorter',
        positionNumber: 5,
        salary: 1000,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        id: 20,
        name: 'Daniil',
        surname: 'Mason',
        position: 'loader',
        positionNumber: 4,
        salary: 950,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        id: 21,
        name: 'Maksim',
        surname: 'Oldman',
        position: 'loader',
        positionNumber: 4,
        salary: 1050,
        works: false,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
]

class RestaurantData {
    constructor(param){
        this.employeeObjects = param;
    }

    sumSalariesDepartment(){
        let sum = {};

        this.employeeObjects.map((item) => {
            sum[item.departmentName] = sum[item.departmentName] || 0;
            sum[item.departmentName] += item.salary;
        });

        return sum;
    }

    averageSalaryDepartment(){
        let meanSalary = {};

        this.employeeObjects.map((item) => {
            meanSalary[item.departmentName] = meanSalary[item.departmentName] || [];

            for(let index in meanSalary){
                if(index === item.departmentName){
                    meanSalary[index].push(item.salary);
                }
            }
        });

        function average(name, items) {
            let sum = 0;

            for (let i = 0; i < items.length; i++) {
              sum += items[i];
            }

            meanSalary[name] = Math.floor(sum / items.length);
        }
        
        for(let index in meanSalary){
            average(index, meanSalary[index]);
        }

        return meanSalary;
    }

    SalarysDepartment(colback){
        let departmentName = {};

        this.employeeObjects.map((item) => {
            departmentName[item.departmentName] = departmentName[item.departmentName] || [];
            for(let index in departmentName){
                if(index === item.departmentName){
                    departmentName[index].push(item.salary);
                }
            }
        });

        for(let index in departmentName){
            departmentName[index].sort(colback);
            departmentName[index] = departmentName[index][0];
        }

        return departmentName;
    }
        
    SalarysPosition(colback){
        let position = {};
        this.employeeObjects.map((item) => {
            position[item.position] = position[item.position] || [];
            for(let index in position){
                if(index === item.position){
                    position[index].push(item.salary);
                }
            }
        });

        for(let index in position){
            position[index].sort(colback);
            position[index] = position[index][0];
        }

        return position;
    }
        
    countDismissed(){
        let result = {};

        this.employeeObjects.map((item) => {
            result[item.departmentName] = result[item.departmentName] || 0;
            result.length = result.length || 0;

            if(item.works === false){
                ++result[item.departmentName];
                ++result.length;
            }
        });

        return result;
    }
        
    departmentsNoManager(){
        let numbDepartament = [];
        let mass = [];
        let result = [];

        this.employeeObjects.map((item) => {
            if(item.positionNumber === 1 && item.works){
                numbDepartament.push(item.departmentNumber);
            }

            mass.push(item);
        });

        for(let index of numbDepartament){
            for(let i = 0; i < mass.length; i++){
                if(mass[i].departmentNumber === index){
                    mass.splice(i--, 1);
                }
            }
        }

        for(let index in mass){
            result.push(mass[index].departmentName);
        }

        return result = [...new Set(result)];
    }
}

let restaurantData = new RestaurantData(employeeObjectss);
