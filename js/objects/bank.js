let bankClients = [{
        id: 1,
        name: 'Alex',
        surname: 'Alexis',
        activeClients: false,
        registrationDate: '12.09.2020',
        check: {
            debit: {
                balance: 20000,
                dateIssue: '12.09.2020',
                endTerm: '12.09.2025',
                currency: 'usd',
                active: true,
            },
            credit: {
                balance: 5000,
                creditLimit: 10000,
                dateIssue: '12.09.2021',
                endTerm: '12.09.2026',
                currency: 'rub',
                active: false,
            },
        }
    },
    {
        id: 2,
        name: 'Aleksandr',
        surname: 'Hoggarth',
        activeClients: true,
        registrationDate: '20.01.2015',
        check: {
            debit: {
                balance: 0,
                dateIssue: '25.01.2015',
                endTerm: '25.01.2020',
                currency: 'rub',
                active: false,
            },
            credit: {
                balance: 5000,
                creditLimit: 10000,
                dateIssue: '25.05.2015',
                endTerm: '25.05.2020',
                currency: 'usd',
                active: true,
            },
        }
    },
    {
        id: 3,
        name: 'Kirill',
        surname: 'Nash',
        activeClients: false,
        registrationDate: '20.01.2010',
        check: {
            debit: {
                balance: 0,
                dateIssue: '25.01.2011',
                endTerm: '25.01.2016',
                currency: 'eur',
                active: false,
            },
            credit: {
                balance: 12000,
                creditLimit: 20000,
                dateIssue: '25.05.2011',
                endTerm: '25.05.2016',
                currency: 'usd',
                active: true,
            },
        }
    },
    {
        id: 4,
        name: 'Aleksey',
        surname: 'Holiday',
        activeClients: true,
        registrationDate: '20.01.2021',
        check: {
            debit: {
                balance: 50000,
                dateIssue: '25.01.2021',
                endTerm: '25.01.2026',
                currency: 'rub',
                active: true,
            },
            credit: {
                balance: 24000,
                creditLimit: 30000,
                dateIssue: '25.05.2021',
                endTerm: '25.05.2026',
                currency: 'eur',
                active: true,
            },
        }
    },
    {
        id: 5,
        name: 'Anatoly',
        surname: 'James',
        activeClients: true,
        registrationDate: '20.01.2019',
        check: {
            debit: {
                balance: 10000,
                dateIssue: '25.01.2019',
                endTerm: '25.01.2024',
                currency: 'eur',
                active: true,
            },
            credit: {
                balance: 5000,
                creditLimit: 5000,
                dateIssue: '25.05.2019',
                endTerm: '25.05.2024',
                currency: 'eur',
                active: true,
            },
        }
    },
]

class Bank {
    constructor(param) {
        this.bankClients = param;
    }

    serviceRequest(param, callback) {
        let currency = 'rub';
        let exception = currency + currency;

        let url = "https://evgeniychvertkov.com/api/exchange/?";

        for (let index in param) {
            if (index !== (currency + currency)) {
                url += 'currency[]=' + index + '&';
            }
        }

        return fetch(url, {
            method: "GET",
            headers: {
                "X-Authorization-Token": "ce46960e-3005-11ec-a1b1-8a04c6a70bd3",
                "Content-Type": "application/json; charset=utf-8"
            }
        }).then((data) => data.json()).then((obj) => {
            return callback(obj, exception, param);
        });
    }

    totalAmountMoney() {
        let param = {};
        let currency = 'rub';

        this.bankClients.forEach(item => {
            param[item.check.debit.currency + currency] = param[item.check.debit.currency + currency] || 0;
            param[item.check.credit.currency + currency] = param[item.check.credit.currency] || 0;

            param[item.check.debit.currency + currency] += item.check.debit.balance;
            param[item.check.credit.currency + currency] += item.check.credit.balance;
        });

        return this.serviceRequest(param, (obj, exception, param) => {
            let result = param[exception] || 0;

            for (let index in obj.data) {
                result += (param[index] * obj.data[index]);
            }

            return Math.floor(result * 100) / 100;
        });
    }

    oweBank() {
        let param = {};
        let currency = 'rub';

        this.bankClients.forEach(item => {
            if (item.check.credit.creditLimit >= item.check.credit.balance) {
                param[item.check.credit.currency + currency] = param[item.check.credit.currency + currency] || 0;
                param[item.check.credit.currency + currency] += item.check.credit.creditLimit - item.check.credit.balance;
            }
        });

        return this.serviceRequest(param, (obj, exception, param) => {
            let result = param[exception] || 0;

            for (let index in obj.data) {
                result += (param[index] * obj.data[index]);
            }

            return Math.floor(result * 100) / 100;
        });
    }

    CustomersOweBank(argument) {
        let param = {};
        let counterInactive = 0;
        let currency = 'rub';

        this.bankClients.forEach(item => {
            if (item.check.credit.creditLimit >= item.check.credit.balance && item.activeClients === argument) {
                ++counterInactive
                param[item.check.credit.currency + currency] = param[item.check.credit.currency + currency] || 0;
                param[item.check.credit.currency + currency] += item.check.credit.creditLimit - item.check.credit.balance;
            }
        });

        return this.serviceRequest(param, (obj, exception, param) => {
            let result = {
                length: counterInactive,
                indebtedness: param[exception] || 0,
            };

            for (let index in obj.data) {
                result.indebtedness += (param[index] * obj.data[index]);
            }

            return Math.floor(result.indebtedness * 100) / 100;
        });
    }
}

const bank = new Bank(bankClients);
